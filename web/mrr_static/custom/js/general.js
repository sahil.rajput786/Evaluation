var site_viewer = document.getElementById("site-identifier");
var csrf_token = site_viewer.getAttribute("data-token");




/* Function: Round all numbers in an array and then add commas if needed for styling */
function roundArray(array, commas) {
	array.forEach(function(part, index) {
		array[index] = rounder(part, commas);
	});
  
	return array;
}

/* Function: Round numbers to the closest number and then add commas if needed for styling */
function rounder(num, commas) {
	if (num) {
    if (commas) {
		  return addCommas(Math.round(num));
    } else {
      return Math.round(num);
		}
  } else
		return 0
}

/* Function: Add commas to a numerical number */
function addCommas(input) {
  // If the regex doesn't match, `replace` returns the string unmodified
  return (input.toString()).replace(
    // Each parentheses group (or 'capture') in this regex becomes an argument
    // to the function; in this case, every argument after 'match'
    /^([-+]?)(0?)(\d+)(.?)(\d+)$/g, function(match, sign, zeros, before, decimal, after) {

      // Less obtrusive than adding 'reverse' method on all strings
      var reverseString = function(string) { return string.split('').reverse().join(''); };

      // Insert commas every three characters from the right
      var insertCommas  = function(string) {

        // Reverse, because it's easier to do things from the left
        var reversed = reverseString(string);

        // Add commas every three characters
        var reversedWithCommas = reversed.match(/.{1,3}/g).join(',');

        // Reverse again (back to normal)
        return reverseString(reversedWithCommas);
      };

      // If there was no decimal, the last capture grabs the final digit, so
      // we have to put it back together with the 'before' substring
      return sign + (decimal ? insertCommas(before) + decimal + after : insertCommas(before + after));
    }
  );
}


/* Function: Create a table out of the variables coming in */
/* Can handle mixed graphs.
   Parameters: chart_canvas: ID of where graph should be placed
         x_data: x-axis labeling
         y_matrix: contains data */
function insertGrid(chart_grid, x_data, y_matrix) {

  // Enter code below...
var x_output=x_data.map(function(x){

this.x_data_input = $("<th></th>").text(x);
  return (this.x_data_input);

})

var grid_output=y_matrix.map(function(obj){
this.x_data_input = $("<tr></tr>");
    this.dataRow=obj.data.map(function(x){
this.x_data_input1 = $("<td></td>");
     
      this.x_data_input1.text(x);
      return (this.x_data_input1);
    })
    for (var i =0  ; i <= this.dataRow.length - 1; i++) {
      this.dataRow[i]
      this.x_data_input.append(this.dataRow[i]);
    }

return (this.x_data_input);
})

chart_grid.append('<div class="table-responsive"><table class="table table-bordered"><thead><tr  class="head"></tr><thead><tbody class="body"></tbody>');
  chart_grid.find(".head").append(x_output);
  chart_grid.find(".body").append(grid_output);
}