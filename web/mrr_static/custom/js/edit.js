$(document).ready( function() {

	$(".btn-link.glyphicon-pencil").click(function() {
		var my_tr = $(this).closest("tr");

		$(this).addClass("hidden");
		my_tr.find("td.editable").addClass("edit-on");
		my_tr.find("td.editable .td-label").addClass("hidden");
		my_tr.find("td.editable .td-input").removeClass("hidden");
		my_tr.find(".glyphicon-floppy-disk.hidden").removeClass("hidden");

		$("#process-data").attr("disabled", "disabled");
	});

	$(".btn-link.glyphicon-floppy-disk").click(function() {
		var my_tr = $(this).closest("tr");

		saveFunction(my_tr);

		$("#process-data").removeAttr("disabled");
	});

	$("table").on("click", "td.edit-on", function() {
		$(this).addClass("td-updated");
	});

	$("#process-data").click(function() {
		var error_need_value = false;
		$(".table-responsive input[data-required]").each(function() {
			if ($(this).val() == "")
				error_need_value = true;
		});

		if (error_need_value) {
			alert("Missing a start or end date.");
		} else {
			window.location.href = "/process_data/";
		}
	});
});



function saveFunction(tr) {
	var id = tr.attr('data-id');
	var changed_values = tr.find("td.td-updated input, td.td-updated select");
	var packet = {};
	var val_changed = false;
	var errors = false;
	var url = "/update_csv/" + id + "/";

	// Look at all the changed values
	changed_values.each(function() {

		var model_field = $(this).attr("data-type");
		var label_required = $(this).attr("data-required");
		var field_value = $(this).val();

		if ((!field_value || field_value == "") && label_required !== undefined) {
			errors = true;
		}

		packet[model_field] = field_value;
		val_changed = true;

	});

	if (errors) {
		alert("Missing a start or end date.");
		return;
	}

	// If values did change, submit a post request for that column
	if (val_changed) {
		$.ajax({
	        type : "POST",
	        beforeSend: function(xhr, settings) {
				xhr.setRequestHeader("X-CSRFToken", csrf_token);
			},
	        url : url,
	        data : packet,
	        dataType : "json",
	        success: function(data) {
	        	var updated_changes = data[0].fields;

	        	var new_customer = updated_changes.new_customer;
	        	var start_date = updated_changes.start_date;
	        	var start_date_object = new Date(start_date);
	        	var end_date = updated_changes.end_date;
	        	var end_date_object = new Date(end_date);
	        	var product_class = updated_changes.product_class;
	        	var revenue_type = updated_changes.revenue_type;

	        	
	        	if (new_customer) {
	        		tr.find("td[data-type='new_customer'] .td-label").text("True");
	        		tr.find("td[data-type='new_customer'] select").val("true");
	        	} else {
	        		tr.find("td[data-type='new_customer'] .td-label").text("False");
	        		tr.find("td[data-type='new_customer'] select").val("false");
	        	}
        		
        		if (start_date)
        			tr.find("td[data-type='start_date'] .td-label").text(getFormattedDate(start_date_object));
        		else
        			tr.find("td[data-type='start_date'] .td-label").text("");
        		tr.find("td[data-type='start_date'] input").val(start_date);
        		
        		if (end_date)
        			tr.find("td[data-type='end_date'] .td-label").text(getFormattedDate(end_date_object));
        		else
        			tr.find("td[data-type='end_date'] .td-label").text("");
        		tr.find("td[data-type='end_date'] input").val(end_date);
        		
        		tr.find("td[data-type='product_class'] .td-label").text(product_class);
        		tr.find("td[data-type='product_class'] input").val(product_class);
        		
        		tr.find("td[data-type='revenue_type'] .td-label").text(revenue_type);
        		tr.find("td[data-type='revenue_type'] input").val(revenue_type);

        		tr.find("td.editable").removeClass("edit-on");
				tr.find("td.editable .td-label").removeClass("hidden");
				tr.find("td.editable .td-input").addClass("hidden");
				tr.find(".glyphicon-pencil.hidden").removeClass("hidden");
				tr.find(".glyphicon-floppy-disk").addClass("hidden");
	        },
	        error: function (xhr, ajaxOptions, thrownError) {
	        	alert("Error trying to save new data.");
	        }
	    });
	} else {

		tr.find("td.editable").removeClass("edit-on");
		tr.find("td.editable .td-label").removeClass("hidden");
		tr.find("td.editable .td-input").addClass("hidden");
		tr.find(".glyphicon-pencil.hidden").removeClass("hidden");
		tr.find(".glyphicon-floppy-disk").addClass("hidden");

	}
}


function getFormattedDate(date) {
	var year = date.getUTCFullYear();
	var month = (1 + date.getUTCMonth()).toString();
	var day = date.getUTCDate().toString();

	month = month.length > 1 ? month : '0' + month;
	day = day.length > 1 ? day : '0' + day;
	return month + '/' + day + '/' + year;
}