# -*- coding: utf-8 -*-
# Generated by Django 1.10.6 on 2017-08-22 17:49
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='ClientMrr',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('mrr', models.DecimalField(decimal_places=5, max_digits=12)),
                ('month', models.DateField()),
                ('classification', models.CharField(max_length=25)),
                ('classification_value', models.DecimalField(decimal_places=5, max_digits=12, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='CsvUpload',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date_uploaded', models.DateTimeField(auto_now_add=True)),
                ('title', models.CharField(max_length=100)),
            ],
        ),
        migrations.CreateModel(
            name='Customer',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('internal_name', models.CharField(max_length=150, unique=True)),
                ('name', models.CharField(max_length=100, null=True)),
                ('category', models.CharField(max_length=50, null=True)),
                ('primary_lip', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='mrr_calculations.Customer')),
            ],
        ),
        migrations.CreateModel(
            name='InvoiceItem',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('invoice_number', models.CharField(max_length=50)),
                ('invoice_date', models.DateField()),
                ('transaction_type', models.CharField(max_length=50)),
                ('product_type', models.CharField(max_length=100)),
                ('quantity', models.DecimalField(decimal_places=5, max_digits=12, null=True)),
                ('rate', models.DecimalField(decimal_places=5, max_digits=12, null=True)),
                ('amount', models.DecimalField(decimal_places=5, max_digits=12, null=True)),
                ('memo', models.TextField(null=True)),
                ('is_processed', models.BooleanField(default=False)),
                ('start_date', models.DateField(null=True)),
                ('end_date', models.DateField(null=True)),
                ('product_class', models.CharField(max_length=50)),
                ('revenue_type', models.CharField(max_length=50)),
                ('customer', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='mrr_calculations.Customer')),
                ('orig_csv', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='mrr_calculations.CsvUpload')),
            ],
        ),
        migrations.AddField(
            model_name='clientmrr',
            name='customer',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='mrr_calculations.Customer'),
        ),
    ]
