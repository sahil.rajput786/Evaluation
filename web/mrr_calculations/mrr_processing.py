import csv
import datetime
import re
from decimal import Decimal

from dateutil import relativedelta

from django.db.models import Sum, Case, When, CharField, Max, DateField, DecimalField, Q, Value
from django.db.models.functions import Trunc, Coalesce

from mrr_calculations.models import Customer, CsvUpload, InvoiceItem, ClientMrr


non_decimal = re.compile(r'[^\d.]+')

# Generates a dictionary of the form YYYY_MM:ClientMrr object
# Used to keep track of the running MRR total
class MrrMonth:
    # CHOICE MAPPINGS HERE - like for classification (new, churn, etc)

    def __init__(self, start_date,end_date, mrr_objects, customer):
        self.start_date = start_date
        self.end_date = end_date
        self.customer = customer
        self.mrr_objects = mrr_objects

        print "START DATE END DATE", start_date, end_date

        month_dict = {}
        current_date = start_date
        while current_date <= end_date:
            month_dict[current_date.strftime('%Y_%m')] = ClientMrr(customer_id=customer, mrr=0, month=current_date)
            current_date += relativedelta.relativedelta(months=1)

        for mrr_object in mrr_objects:
            month_dict[mrr_object.month.strftime('%Y_%m')] = mrr_object
        self.mrr_dict = month_dict


    def __str__(self):
        s = 'MRR for client %s - ' % self.customer
        for month in sorted(self.mrr_dict.keys()):
            s += "%s\n" % (self.mrr_dict[month])
        return s

    def add_mrr(self, invoice_item):
        month_count = MrrMonth.calculate_months(invoice_item.start_date,invoice_item.end_date)
        mrr_amount = invoice_item.amount/month_count

        if month_count == 1:
            self.mrr_dict[invoice_item.start_date.strftime('%Y_%m')].mrr += mrr_amount
        else:
            # for each month, add mrr_amount to its clientmrr objects
            current_month = invoice_item.start_date
            end_month = invoice_item.end_date
            # TODO CHECK IF A X/1-X/31 IS GETTING COUNTED RIGHT
            if (invoice_item.end_date + datetime.timedelta(days=1)).month == invoice_item.end_date.month:
                end_month = invoice_item.end_date.replace(day=1)
            while current_month < end_month:
                self.mrr_dict[current_month.strftime('%Y_%m')].mrr += mrr_amount
                current_month += relativedelta.relativedelta(months=1)

    # Given a dictionary of mrr objects
    # Classify its type - expansion, contraction, churn, etc
    # MAY HAVE TO WRITE ANOTHER FUNCTION CALLED FIND_CHURNS TO CREATE mrr=0 ROWS FOR CHURNED CUSTOMERS
    # OR A FUNCTION TO PRUNE MULTIPLE STRETCHES OF 0 ROW MRRS (mrr=0 and classificati)
    def classify_mrr(self):
        prev_mrr = None
        prev_month = None
        for month in sorted(self.mrr_dict.keys()):
            if prev_mrr is not None:

                # IF IT'S THE LAST MONTH, DON'T DO ANYTHING
                if prev_mrr == 0:
                    self.mrr_dict[month].classification = "new"
                    self.mrr_dict[month].classification_value = self.mrr_dict[month].mrr
                elif self.mrr_dict[month].mrr == 0:
                    self.mrr_dict[month].classification = "churn"
                    self.mrr_dict[month].classification_value = prev_mrr * -1
                elif self.mrr_dict[month].mrr < prev_mrr:
                    self.mrr_dict[month].classification = "contraction"
                    self.mrr_dict[month].classification_value = self.mrr_dict[month].mrr - prev_mrr
                elif self.mrr_dict[month].mrr > prev_mrr:
                    self.mrr_dict[month].classification = "expansion"
                    self.mrr_dict[month].classification_value = self.mrr_dict[month].mrr - prev_mrr
                else:
                    self.mrr_dict[month].classification = ""
                    self.mrr_dict[month].classification_value = None

            print "MRR ", self.mrr_dict[month].mrr
            prev_mrr = self.mrr_dict[month].mrr
            prev_month = month


    def save_mrr(self):
        first_row = True
        for key in sorted(self.mrr_dict.keys()):
            if first_row:
                first_row = False
                continue
            self.mrr_dict[key].save()
        InvoiceItem.objects.filter(is_processed=False, customer_id=self.customer).update(is_processed=True)

    # Return the number of months between 2 dates
    # For our purposes, we don't count the end month, ie 5/15-6/15 counts as 1 month
    # Exception: 5/1-5/31 would count as 1, not 0 months
    # Exception: dates ending on the last day of the month count it too, ie 5/15-6/30 - 2 months
    @staticmethod
    def calculate_months(start_date, end_date):
        start_date = start_date.replace(day=1)
        new_end_date = end_date.replace(day=1)
        r = relativedelta.relativedelta(new_end_date, start_date)
        month_count = r.months + (12 * r.years)

        # if this is the last day of the month, count it too
        if (end_date + datetime.timedelta(days=1)).month != end_date.month:
            month_count += 1

        return month_count if month_count > 0 else 1


def convert_input_to_decimal(input_string):
    stripped_input = non_decimal.sub('',input_string)
    if stripped_input:
        converted_number = Decimal(stripped_input)
    else:
        converted_number = 0.0

    if ('(' in input_string or '-' in input_string) and converted_number != 0:
        converted_number *= -1
    return converted_number


def process_csv(csv_file, filename):
    reader = csv.DictReader(csv_file)
    csv_entry = CsvUpload.objects.create(title=filename)
    invoice_list = []

    for row in reader:
        # If we need to, we can speed this up by keeping these ids in a set (if the data has lots of repeated customers)
        current_customer, is_new = Customer.objects.get_or_create(internal_name=row["Client"])

        start_date, end_date = None, None
        if row['Start Date']:
            start_date = datetime.datetime.strptime(row["Start Date"], "%m/%d/%Y").date()
            end_date = datetime.datetime.strptime(row["End Date"], "%m/%d/%Y").date()

            if end_date < start_date:
                raise ValueError("Bad data - end date before start date ({0}, {1})".format(start_date, end_date))

# Date,Transaction Type,Num,Client,Product/Service,Memo/Description,Qty,Rate,Amount,Type of Revenue,Type of Customer,Start Date,End Date,Product Class
        new_invoice = InvoiceItem(
            invoice_number=row["Num"],
            invoice_date=datetime.datetime.strptime(row["Date"], "%m/%d/%Y").date(),
            transaction_type=row["Transaction Type"],
            product_type=row["Product/Service"],
            quantity=convert_input_to_decimal(row["Qty"]),
            rate=convert_input_to_decimal(row[" Rate "]),
            amount=convert_input_to_decimal(row[" Amount "]),
            memo=row["Memo/Description"],
            revenue_type=row["Type of Revenue"],
            customer=current_customer,
            start_date=start_date,
            end_date=end_date,
            orig_csv=csv_entry
        )
        invoice_list.append(new_invoice)

    InvoiceItem.objects.bulk_create(invoice_list)

    return invoice_list


# This function runs after we classify InvoiceItems
# It pulls all InvoiceItems that have a False is_processed flag
# If we find we don't have enough info to process it, we reset the flag
def process_new_mrr():
    customers = InvoiceItem.objects.filter(is_processed=False).values_list('customer_id',flat=True).distinct()
    for customer_id in customers:
        # ANNOTATE MIN AND MAX START/END TIMES
        invoice_items = InvoiceItem.objects.filter(customer_id=customer_id,is_processed=False).order_by('start_date')
        invoice_start_date = invoice_items.first().start_date.replace(day=1)
        invoice_start_date -= relativedelta.relativedelta(months=1)

        # We need mrr data for the current customer starting from the month before what we're getting revenue for
        # We're going to calculate the MRR number for all the months first, then classify it
        invoice_end_date = InvoiceItem.objects.filter(is_processed=False,customer_id=customer_id).aggregate(Max('end_date'))['end_date__max']

        # We need to check for items that end on the last day of the month - they're special
        #
        if (invoice_end_date + datetime.timedelta(days=1)).month != invoice_end_date.month and invoice_start_date.day != invoice_end_date.day:
            invoice_end_date += relativedelta.relativedelta(months=1)

        invoice_end_date.replace(day=1)

        mrr_objects = ClientMrr.objects.filter(customer_id=customer_id,month__gte=(invoice_start_date)).order_by('month')

        month_dict = MrrMonth(invoice_start_date, invoice_end_date, mrr_objects, customer_id)
        for item in invoice_items:
            # We only want to add recurring revenue, not one-time stuff
            if "revrec" in item.revenue_type.lower():
                month_dict.add_mrr(item)

        month_dict.classify_mrr()
        month_dict.save_mrr()

        print month_dict

# Takes a start and end time and fills in missing records
# IE, when there's no data for a given month
def create_mrr_resultset(start_date, end_date, mrr_list):
    current_date = start_date
    month_list = []
    mrr_index = 0

    while current_date < end_date:
        if mrr_index < len(mrr_list) and mrr_list[mrr_index]['month'] == current_date:
            month_list.append(mrr_list[mrr_index])
            mrr_index += 1
        else:
            month_list.append({'month':current_date})

        current_date += relativedelta.relativedelta(months=1)

    return month_list

# This takes the list of mrr totals and adds the non-recurring revenue from that month
def create_bookings_resultset(mrr_list, invoice_items):
    invoice_index = 0
    for mrr_row in mrr_list:
        if invoice_index < len(invoice_items) and mrr_row['month'] == invoice_items[invoice_index]['start_month']:
            mrr_row['bookings'] = invoice_items[invoice_index]['bookings']
            invoice_index += 1
        else:
            mrr_row['bookings'] = 0
    return mrr_list

# Takes in kwargs and returns rows of valuesqueryset for the timeframe
def query_mrr(start_date,end_date,**kwargs):
    if start_date is None:
        start_date = (datetime.datetime.now().date() - relativedelta.relativedelta(years=1)).replace(day=1)
    if end_date is None:
        end_date = datetime.datetime.now().date().replace(day=1)

    mrr_types = ClientMrr.objects.filter(month__gt=start_date,month__lt=end_date, **kwargs).values('month').annotate(total=Sum('mrr')) \
        .annotate(new_mrr=Coalesce(Sum(Case(When(classification='new',then='mrr'),output_field=CharField())),Value(0))) \
        .annotate(churn_mrr=Coalesce(Sum(Case(When(classification='churn',then='classification_value'),output_field=CharField())),Value(0))) \
        .annotate(expansion_mrr=Coalesce(Sum(Case(When(classification='expansion',then='classification_value'),output_field=CharField())),Value(0))) \
        .annotate(contraction_mrr=Coalesce(Sum(Case(When(classification='contraction',then='classification_value'),output_field=CharField())),Value(0))) \
        .order_by('month')

    mrr_list = create_mrr_resultset(start_date, end_date, mrr_types)
    return mrr_list

# Bookings is month's mrr + non recurring revenue, so we have to get the mrr first
def query_bookings(start_date=None, end_date=None,month_list=None,**kwargs):
    if start_date is None:
        start_date = (datetime.datetime.now().date() - relativedelta.relativedelta(years=1,months=1)).replace(day=1)
    if end_date is None:
        end_date = datetime.datetime.now().date()

    if month_list is None:
        month_list = query_mrr(start_date, end_date, **kwargs)

    invoice_items = InvoiceItem.objects.filter(is_processed=True,invoice_date__gte=start_date,invoice_date__lt=end_date, **kwargs) \
        .annotate(start_month=Trunc('invoice_date','month',output_field=DateField())) \
        .values('start_month').annotate(bookings=Sum('amount')).order_by('start_month')

    bookings_list = create_bookings_resultset(month_list, invoice_items)

    return bookings_list
